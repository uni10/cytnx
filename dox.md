# Cytnx

## Current Version:
    v0.5

## What's new:
    1. Add .imag() .real() for Storage. 
    2. Add xlinalg under cytnx_extension for algebra on CyTensor
    3. Add xlinalg::Svd()  

## Version log
    v0.4
    1. remove Otimes, add Kron and Outer 
    2. Add Storage append, capacity, pre-alloc 32x address
    3. Tensor can now allow redundant dimension (e.g. shape = (1,1,1,1,1...) 
    4. Add Storage.from_vector, directly convert the C++ vector to Storage
    5. Add more intruisive way to get slices for Tensor in C++, using operator[]
    6. Add Tensor.append for rank-1 Tensor    
    7. Add Exp() Expf() Exp\_() Expf\_()
    8. Change UniTensor to CyTensor 
    9. Guarded CyTensor, Bond, Symmetry and Network class with cytnx_extension namespace (cytnx_extension submodule in python).  
       

## Feature:

### Python x C++
    Benefit from both side. 
    One can do simple prototype on python side 
    and easy transfer to C++ with small effort!


```{.cpp}

    // c++ version:
    #include "cytnx.hpp"
    cytnx::Tensor A({3,4,5},cytnx::Type.Double,cytnx::Device.cpu)


    
```


```{.py}

    # python version:
    import cytnx
    A =  cytnx.Tensor([3,4,5],dtype=cytnx.Type.Double,device=cytnx.Device.cpu)



```

### 1. All the Storage and Tensor can now have mulitple type support. 
    The avaliable types are :

    | cytnx type       | c++ type             | Type object
    |------------------|----------------------|--------------------
    | cytnx_double     | double               | Type.Double
    | cytnx_float      | float                | Type.Float
    | cytnx_uint64     | uint64_t             | Type.Uint64
    | cytnx_uint32     | uint32_t             | Type.Uint32
    | cytnx_uint16     | uint16_t             | Type.Uint16
    | cytnx_int64      | int64_t              | Type.Int64
    | cytnx_int32      | int32_t              | Type.Int32
    | cytnx_int16      | int16_t              | Type.Int16
    | cytnx_complex128 | std::complex<double> | Type.ComplexDouble
    | cytnx_complex64  | std::complex<float>  | Type.ComplexFloat
    | cytnx_bool       | bool                 | Type.Bool

### 2. Multiple devices support.
    * simple moving btwn CPU and GPU (see below)


## Objects:
    * \link cytnx::Storage Storage \endlink   [binded]
    * \link cytnx::Tensor Tensor \endlink   [binded]
    * \link cytnx::Bond Bond \endlink     [binded] 
    * \link cytnx::Accessor Accessor \endlink [c++ only]
    * \link cytnx::Symmetry Symmetry \endlink [binded]
    * \link cytnx::CyTensor CyTensor \endlink [binded]
    * \link cytnx::Network Network \endlink [binded]

## linear algebra functions: 
    See \link cytnx::linalg cytnx::linalg \endlink for further details

      func    |   inplace | CPU | GPU  | callby tn 
    ----------|-----------|-----|------|-----------
      \link cytnx::linalg::Add Add\endlink     |   x       |  Y  |  Y   |    Y
      \link cytnx::linalg::Sub Sub\endlink     |   x       |  Y  |  Y   |    Y
      \link cytnx::linalg::Mul Mul\endlink     |   x       |  Y  |  Y   |    Y
      \link cytnx::linalg::Div Div\endlink     |   x       |  Y  |  Y   |    Y
      \link cytnx::linalg::Cpr Cpr\endlink     |   x       |  Y  |  Y   |    Y
      +,+=[tn]|   x       |  Y  |  Y   |    Y (\link cytnx::Tensor::Add_ Tensor.Add_\endlink)
      -,-=[tn]|   x       |  Y  |  Y   |    Y (\link cytnx::Tensor::Sub_ Tensor.Sub_\endlink)
      *,*=[tn]|   x       |  Y  |  Y   |    Y (\link cytnx::Tensor::Mul_ Tensor.Mul_\endlink)
      /,/=[tn]|   x       |  Y  |  Y   |    Y (\link cytnx::Tensor::Div_ Tensor.Div_\endlink)
      ==  [tn]|   x       |  Y  |  Y   |    Y (\link cytnx::Tensor::Cpr_ Tensor.Cpr_\endlink)
      \link cytnx::linalg::Svd Svd\endlink     |   x       |  Y  |  Y   |    Y
      *\link cytnx::linalg::Svd_truncate Svd_truncate\endlink     |   x       |  Y  |  Y   |    N
      \link cytnx::linalg::Inv Inv\endlink     |   \link cytnx::linalg::Inv_ Inv_\endlink    |  Y  |  Y   |    Y
      \link cytnx::linalg::Conj Conj\endlink    |   \link cytnx::linalg::Conj_ Conj_\endlink   |  Y  |  Y   |    Y 
      \link cytnx::linalg::Exp Exp\endlink     |   \link cytnx::linalg::Exp_ Exp_\endlink    |  Y  |  Y   |    Y
      \link cytnx::linalg::Expf Expf\endlink     |   \link cytnx::linalg::Expf_ Expf_\endlink    |  Y  |  Y   |    Y
      \link cytnx::linalg::Eigh Eigh\endlink    |   x       |  Y  |  Y   |    Y 
      \link cytnx::linalg::Matmul Matmul\endlink  |   x       |  Y  |  Y   |    N
      \link cytnx::linalg::Diag Diag\endlink    |   x       |  Y  |  Y   |    N
      *\link cytnx::linalg::Tensordot Tensordot\endlink | x | Y | Y | N
      \link cytnx::linalg::Outer Outer\endlink  |   x       | Y   | Y   |   N
      \link cytnx::linalg::Kron Kron\endlink  |   x       | Y   | N   |   N
      \link cytnx::linalg::Vectordot Vectordot\endlink    |   x       |  Y  |  N   |    N
      \link cytnx::linalg::Tridiag Tridiag\endlink    | x | Y | N | N  
    *this is a high level linalg 
    ^this is temporary disable

## Generators 
    Tensor: \link cytnx::zeros zeros()\endlink, \link cytnx::ones ones()\endlink, \link cytnx::arange arange()\endlink
    
## Requirements
    * Boost v1.53+ [check_deleted, atomicadd, intrusive_ptr]
    * C++11
    * lapack 
    * blas 
    * gcc v4.8.5+ (recommand v6+) (required -std=c++11)

    [CUDA support]
    * CUDA v10+
    * cuDNN

    [OpenMp support]
    * openmp

    [Python]
    * pybind11 2.2.4
    * numpy >= 1.15 


## conda install  
    [Currently Linux only]

    without CUDA
    * python 3.6: conda install -c kaihsinwu cytnx_36
    * python 3.7: conda install -c kaihsinwu cytnx_37

    with CUDA
    * python 3.6: conda install -c kaihsinwu cytnx_cuda_36
    * python 3.7: conda install -c kaihsinwu cytnx_cuda_37


## docker image with MKL 
  [https://hub.docker.com/r/kaihsinwu/cytnx_mkl](https://hub.docker.com/r/kaihsinwu/cytnx_mkl)

    
### To run:

```{.sh}
    $docker pull kaihsinwu/cytnx_mkl
    $docker run -ti kaihsinwu/cytnx_mkl
```

###Note:
    Once docker image is run, the user code can be compile (for example) with:

```{.sh}
    $g++-6 -std=c++11 -O3 <your.code.cpp> /opt/cytnx/libcytnx.so
```

## compile
    1.) Set the flags to config the install inside make.inc
        There are 4 important flags: ICPC_Enable, OMP_Enable, GPU_Enable, MKL_Enable  
        * a. The default compiler is g++-6. Change "GCC" for the compiler on your system.
            
            To use intel icpc compiler instead of default compiler, set "ICPC_Enable"=1.

            [Note] You can only choose either icpc or gcc. 
                * In the case where ICPC_Enable=1, GCC will be ignore. 
                * In the case where ICPC_Enable=1, set "ICPC" to the path of your icpc binary.
                * In the case where ICPC_Enable=0, "ICPC" will be ignored. 
                  
        * b. To enable the GPU support, set "GPU_Enable" =1, otherwise =0.  

            [Note] 
                * if GPU_Enable=1, the "CUDA_PATH" should be set to the cuda directory on your system.
                * if GPU_Enable=0, the "CUDA_PATH" will be ignored. 

        * c. To enable the acceleration using OpenMP, set "OMP_Enable" =1, otherwise =0.
        * d. The default linalg library are using LAPACK and BLAS. To use intel MKL instead, set "MKL_enable" =1.
        
    2.) compile by running:
        
        $make -Bj

    3.) [Option] compile the python API
        
        $make pyobj

   
## Some snippets:

### Storage
    * Memory container with GPU/CPU support. 
      maintain type conversions (type casting btwn Storages) 
      and moving btwn devices.
    * Generic type object, the behavior is very similar to python.
```{.cpp}

        Storage A(400,Type.Double);
        for(int i=0;i<400;i++)
            A.at<double>(i) = i;

        Storage B = A; // A and B share same memory, this is similar as python 
        
        Storage C = A.to(Device.cuda+0); 

```

### Tensor
    * A tensor, API very similar to numpy and pytorch.
    * simple moving btwn CPU and GPU:
```{.cpp}

        Tensor A({3,4},Type.Double,Device.cpu); // create tensor on CPU (default)
        Tensor B({3,4},Type.Double,Device.cuda+0); // create tensor on GPU with gpu-id=0


        Tensor C = B; // C and B share same memory.

        // move A to gpu
        Tensor D = A.to(Device.cuda+0);

        // inplace move A to gpu
        A.to_(Device.cuda+0);

```
    * Type conversion in between avaliable:
```{.cpp}

        Tensor A({3,4},Type.Double);
        Tensor B = A.astype(Type.Uint64); // cast double to uint64_t

```
    * vitual swap and permute. All the permute and swap will not change the underlying memory
    * Use Contiguous() when needed to actual moving the memory layout.
```{.cpp}

        Tensor A({3,4,5,2},Type.Double);
        A.permute_({0,3,1,2}); // this will not change the memory, only the shape info is changed.
        cout << A.is_contiguous() << endl; // this will be false!

        A.contiguous_(); // call Configuous() to actually move the memory.
        cout << A.is_contiguous() << endl; // this will be true!

```
    * access single element using .at
```{.cpp}

        Tensor A({3,4,5},Type.Double);
        double val = A.at<double>({0,2,2});

```
    * access elements with python slices similarity:
```{.cpp}

        typedef Accessor ac;
        Tensor A({3,4,5},Type.Double);
        Tensor out = A.get({ac(0),ac::all(),ac::range(1,4)}); 
        // equivalent to python: out = A[0,:,1:4]    

```


     
## Fast Examples
    
    See test.cpp for using C++ .
    See test.py for using python  

## Developer

    Kai-Hsin Wu kaihsinwu@gmail.com 




